import socket
import requests
import json
import datetime
LISTEN_PORT = 666
NUM_OF_PACKETS = 500
SERVER_IP = "54.71.128.194"
SERVER_PORT = 8808
STATISTICS = {"bytes_per_ip":{}, "bytes_per_country":{}, "bytes_per_program":{}, "bytes_per_port":{}, "incoming_bytes_per_user":{}, "outgoing_bytes_per_user":{}, "bad_ip_users":[]}

def add_to_general_statistics(packet, client, blacklist_dict):
    """The function adds the packet details to the overall statistics
    :param packet: the details about a single packet
    :param client: the name of the client the packet was sent by
    :param blacklist_dict: a dictionary of all the blacklisted ips and the names of their sites
    :type packet: dict
    :type client: str
    :type blacklist_dict: dict
    :return: none
    :rtype: none
    """
    if packet["IP"] in STATISTICS["bytes_per_ip"].keys():
        STATISTICS["bytes_per_ip"][packet["IP"]] += packet["SIZE"]
    else:
        STATISTICS["bytes_per_ip"][packet["IP"]] = packet["SIZE"]
        
    if packet["COUNTRY"] in STATISTICS["bytes_per_country"].keys():
        STATISTICS["bytes_per_country"][packet["COUNTRY"]] += packet["SIZE"]
    else:
        STATISTICS["bytes_per_country"][packet["COUNTRY"]] = packet["SIZE"]
        
    if packet["PROGRAM"] in STATISTICS["bytes_per_program"].keys():
        STATISTICS["bytes_per_program"][packet["PROGRAM"]] += packet["SIZE"]
    else:
        STATISTICS["bytes_per_program"][packet["PROGRAM"]] = packet["SIZE"]
        
    if packet["PORT"] in STATISTICS["bytes_per_port"].keys():
        STATISTICS["bytes_per_port"][packet["PORT"]] += packet["SIZE"]
    else:
        STATISTICS["bytes_per_port"][packet["PORT"]] = packet["SIZE"]
    
    if packet["PACKET_INCOMING"]:
        if client in STATISTICS["incoming_bytes_per_user"].keys():
            STATISTICS["incoming_bytes_per_user"][client] += packet["SIZE"]
        else:
            STATISTICS["incoming_bytes_per_user"][client] = packet["SIZE"]
    else:
        if client in STATISTICS["outgoing_bytes_per_user"].keys():
            STATISTICS["outgoing_bytes_per_user"][client] += packet["SIZE"]
        else:
            STATISTICS["outgoing_bytes_per_user"][client] = packet["SIZE"]
    
    for bad_ip in blacklist_dict.keys():
        if bad_ip in packet["IP"] and (client, bad_ip) not in STATISTICS["bad_ip_users"]:
            STATISTICS["bad_ip_users"].append((client, bad_ip))
    
def create_report(read_html):
    """The function changes the read_html string details to the updated ones matching the statistics and then creates the new html
    :param read_html: the template html
    :type read_html: str
    :return: none
    :rtype: none
    """
    update_time = datetime.datetime.now().strftime("%d.%m.%Y, %H:%M")
    read_html = read_html[:read_html.find("%%TIMESTAMP%%")-1] + update_time + read_html[read_html.find("%%TIMESTAMP%%")+len("%%TIMESTAMP%%"):]
    read_html = read_html[:read_html.find("%%AGENTS_IN_KEYS%%")-1] + str([*STATISTICS["incoming_bytes_per_user"]]) + read_html[read_html.find("%%AGENTS_IN_KEYS%%")+len("%%AGENTS_IN_KEYS%%"):]
    read_html = read_html[:read_html.find("%%AGENTS_IN_VALUES%%")-1] + str(list(STATISTICS["incoming_bytes_per_user"].values())) + read_html[read_html.find("%%AGENTS_IN_VALUES%%")+len("%%AGENTS_IN_VALUES%%"):]
    read_html = read_html[:read_html.find("%%AGENTS_OUT_KEYS%%")-1] + str([*STATISTICS["outgoing_bytes_per_user"]]) + read_html[read_html.find("%%AGENTS_OUT_KEYS%%")+len("%%AGENTS_OUT_KEYS%%"):]
    read_html = read_html[:read_html.find("%%AGENTS_OUT_VALUES%%")-1] + str(list(STATISTICS["outgoing_bytes_per_user"].values())) + read_html[read_html.find("%%AGENTS_OUT_VALUES%%")+len("%%AGENTS_OUT_VALUES%%"):]
    read_html = read_html[:read_html.find("%%COUNTRIES_KEYS%%")-1] + str([*STATISTICS["bytes_per_country"]]) + read_html[read_html.find("%%COUNTRIES_KEYS%%")+len("%%COUNTRIES_KEYS%%"):]
    read_html = read_html[:read_html.find("%%COUNTRIES_VALUES%%")-1] + str(list(STATISTICS["bytes_per_country"].values())) + read_html[read_html.find("%%COUNTRIES_VALUES%%")+len("%%COUNTRIES_VALUES%%"):]
    read_html = read_html[:read_html.find("%%IPS_KEYS%%")-1] + str([*STATISTICS["bytes_per_ip"]]) + read_html[read_html.find("%%IPS_KEYS%%")+len("%%IPS_KEYS%%"):]
    read_html = read_html[:read_html.find("%%IPS_VALUES%%")-1] + str(list(STATISTICS["bytes_per_ip"].values())) + read_html[read_html.find("%%IPS_VALUES%%")+len("%%IPS_VALUES%%"):]
    read_html = read_html[:read_html.find("%%APPS_KEYS%%")-1] + str([*STATISTICS["bytes_per_program"]]) + read_html[read_html.find("%%APPS_KEYS%%")+len("%%APPS_KEYS%%"):]
    read_html = read_html[:read_html.find("%%APPS_VALUES%%")-1] + str(list(STATISTICS["bytes_per_program"].values())) + read_html[read_html.find("%%APPS_VALUES%%")+len("%%APPS_VALUES%%"):]
    read_html = read_html[:read_html.find("%%PORTS_KEYS%%")-1] + str([*STATISTICS["bytes_per_port"]]) + read_html[read_html.find("%%PORTS_KEYS%%")+len("%%PORTS_KEYS%%"):]
    read_html = read_html[:read_html.find("%%PORTS_VALUES%%")-1] + str(list(STATISTICS["bytes_per_port"].values())) + read_html[read_html.find("%%PORTS_VALUES%%")+len("%%PORTS_VALUES%%"):]
    read_html = read_html[:read_html.find("%%ALERTS%%")-1] + str(STATISTICS["bad_ip_users"]) + read_html[read_html.find("%%ALERTS%%")+len("%%ALERTS%%"):]
    with open("html/toOutput.html", "w") as to_write:
        to_write.write(read_html)

def upload_report():
    """The function uploads the html file previously created to the server by the protocol
    :return: none
    :rtype: none
    """
    with open("html/toOutput.html", "r") as to_read:
        read = to_read.read()
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_address = (SERVER_IP, SERVER_PORT)
    sock.connect(server_address)
    msg = "400#USER=arik.gorun"
    sock.sendall(msg.encode())
    server_msg = sock.recv(1024)
    server_msg = server_msg.decode()
    if "405#USER OK" in server_msg:
        msg = "700#SIZE=" + str(len(read)) + ",HTML=" + read
        sock.sendall(msg.encode())
    sock.close()

def main():
    #call the function find_checksum
    recieved_from = ""
    with open("settings.dat", "r") as to_read:
        read = to_read.readlines()
    workers_list = read[0][10:].split(",")
    black_list = read[1][12:].split(",")
    workers_dict = {}
    blacklist_dict = {}
    for worker in workers_list:
        workers_dict[worker[worker.find(":") + 1:]] = worker[:worker.find(":")]
    for bad_ip in black_list:
        blacklist_dict[bad_ip[:bad_ip.find(":")]] = bad_ip[bad_ip.find(":") + 1:]
    with open("html/template.html", "r") as to_read:
        read_html = to_read.read()
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    server_address = ('', LISTEN_PORT)
    sock.bind(server_address)
    while True:
        client_msg, client_addr = sock.recvfrom(100000)
        print("Message recieved from", workers_dict[client_addr[0]], ":\n")
        client = workers_dict[client_addr[0]]
        for i in range(len(json.loads(client_msg))):
            add_to_general_statistics(json.loads(client_msg)[i], client, blacklist_dict)
            print("\n\npacket num:", str(i+1))
            print("size (in bytes):", str(json.loads(client_msg)[i]["SIZE"]))
            print("ip:", json.loads(client_msg)[i]["IP"])
            print("port:", str(json.loads(client_msg)[i]["PORT"]))
            print("packet incoming:", json.loads(client_msg)[i]["PACKET_INCOMING"])
            print("country:", json.loads(client_msg)[i]["COUNTRY"])
            print("application:", json.loads(client_msg)[i]["PROGRAM"])
        create_report(read_html)
        print("\nmaking report...")
        upload_report()
        print("report uploaded to arik_gorun.bossniffer.com\n")
        print("\n\n")

if __name__ == "__main__":
    main()