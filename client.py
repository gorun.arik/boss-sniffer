import socket
from scapy3k.all import *
import requests
import json
import subprocess
import ctypes, sys
SERVER_ADDR = "127.0.0.1"
IP_CHECK_URL = "http://ip-api.com/json"
PUBLIC_IP_URL = "https://api.ipify.org"
SERVER_PORT = 666
NUM_OF_PACKETS = 500
IP_DICT = {}

def is_admin():
    """The function checks if the program is running with admin permissions
    :return: whether cmd is with admin permissions
    :rtype: bool
    """
    try:
        return ctypes.windll.shell32.IsUserAnAdmin()
    except:
        return False

def packet_is_good(packet):
    """The function determines whether the packet has ip and tcp/udp sections in it
    :param packat: packet sniffed
    :type packet: a packet(scapy)
    :return: whwther or not packet has the requirements
    :rtype: bool
    """
    return (IP in packet and (UDP in packet or TCP in packet))

def netstat(categories_dict):
    """The function runs the netstat command and checks what programs are running on which port using regex
    :param categories_dict: a dictionary with all the packet details
    :type categories_dict: dict
    :return: the updated categories_dict
    :rtype: dict
    """
    p = subprocess.Popen(["netstat", "-nb"], stdout=subprocess.PIPE)
    out = p.stdout.read()
    regex = re.findall(r"\s+(?:TCP|UDP)\s+(?:[0-9]{1,3}\.){3}[0-9]{1,3}:([0-9]{1,5})\s+(?:[0-9]{1,3}\.){3}[0-9]{1,3}:([0-9]{1,5})\s+ESTABLISHED\r\n \[(.+\.exe)\]", out.decode())
    for tuple in regex:
        if tuple[0] == str(categories_dict["PORT"]) or tuple[1] == str(categories_dict["PORT"]):
            categories_dict["PROGRAM"] = tuple[2]
    if "PROGRAM" not in categories_dict:
        categories_dict["PROGRAM"] = "Unknown"
    return categories_dict

def analize_port(categories_dict, packet, kind):
    """The function checks if the packet is incoming or outgoing and decides the port accordingly
    :param categories_dict: a dictionary with all the packet details
    :param kind: the kind of connection (udp/tcp)
	:param packet: a sniffed packet
    :type categories_dict: dict
    :type kind: udp/tcp
	:type packet: str
    :return: the updated categories_dict
    :rtype: dict
    """
    if categories_dict["PACKET_INCOMING"]:
        categories_dict["PORT"] = packet[kind].sport
    else:
        categories_dict["PORT"] = packet[kind].dport
    return categories_dict
    
def analize_packet_way(categories_dict, ip):
    """The function compares the source and destination ip and decides whether the packet is incoming or outgoing
    :param categories_dict: a dictionary with all the packet details
    :param ip: the ips to check
    :type categories_dict: dict
    :type ip: str
    :return: the updated categories_dict and the new public ip if needed
    :rtype: dict, str
    """
    public_ip = ""
    if((ip.src)[:(ip.src).find(".", (ip.src).find(".") + 1)] == (ip.dst)[:(ip.dst).find(".", (ip.dst).find(".") + 1)]):
        public_ip = requests.get(PUBLIC_IP_URL).text
        categories_dict["PACKET_INCOMING"] = (int((ip.src)[(ip.src).find(".", (ip.src).find(".", (ip.src).find(".") + 1) + 1)+ 1:]) < int((ip.dst)[(ip.dst).find(".", (ip.dst).find(".", (ip.dst).find(".") + 1) + 1)+ 1:]))
    elif ("192.168." in ip.dst or "10." in ip.dst or ("172." in ip.dst and 16 <= int((ip.dst)[5:(ip.dst).find(".", 5)]) <= 31)) and ("192.168." in ip.src or "10." in ip.src or ("172." in ip.src and 16 <= int((ip.src)[5:(ip.src).find(".", 5)]) <= 31)):
        categories_dict["PACKET_INCOMING"] = False
    else:
        categories_dict["PACKET_INCOMING"] = ("192.168." in ip.dst or "10." in ip.dst or ("172." in ip.dst and 16 <= int((ip.dst)[5:(ip.dst).find(".", 5)]) <= 31))
    return categories_dict, public_ip
    
def analize_ip(categories_dict, public_ip, ip):
    """The function uses the requests library to check from which country the ip came
    :param categories_dict: a dictionary with all the packet details
    :param ip: the ip to check
	:param public_ip: the public ip
    :type categories_dict: dict
    :type ip: str
	:type public_ip: str
    :return: the updated categories_dict
    :rtype: dict
    """
    categories_dict["IP"] = ip
    if ip not in IP_DICT.keys():
        if len(public_ip) > 0:
            json_string = (requests.get((IP_CHECK_URL + "/" + public_ip)).text)
        else:
            json_string = (requests.get((IP_CHECK_URL + "/" + ip)).text)
        parsed_json = json.loads(json_string)
        if parsed_json["status"] == "success":
            categories_dict["COUNTRY"] = parsed_json["country"]
            IP_DICT[ip] = categories_dict["COUNTRY"]
        else:
            categories_dict["COUNTRY"] = "reserved range"
    else:
        categories_dict["COUNTRY"] = IP_DICT[ip]
    return categories_dict
    
def main():
#call the function find_checksum
    public_ip = ""
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    server_address = (SERVER_ADDR, SERVER_PORT)
    while True:
        data_structure = []
        print("sniffing...")
        packets = sniff(count = NUM_OF_PACKETS, lfilter = packet_is_good)
        print("parsing...")
        for i in range(NUM_OF_PACKETS):
            ip = packets[i][IP]
            categories_dict = {}
            categories_dict["SIZE"] = len(packets[i])
            categories_dict, public_ip = analize_packet_way(categories_dict, ip)   
            
            if TCP in packets[i]:
                categories_dict = analize_port(categories_dict, packets[i], TCP)
            else:
                categories_dict = analize_port(categories_dict, packets[i], UDP)
                    
            if categories_dict["PACKET_INCOMING"]:
                categories_dict = analize_ip(categories_dict, public_ip, ip.src)
            else:
                categories_dict = analize_ip(categories_dict, public_ip, ip.dst)

            categories_dict = netstat(categories_dict)
            data_structure.append(categories_dict)
        sock.sendto(json.dumps(data_structure).encode(),server_address)
        print(NUM_OF_PACKETS, "packets analized sent to server\n")
    sock.close()
    
if __name__ == "__main__":
    if is_admin():
        main()
    else:
        # Re-run the program with admin rights
        ctypes.windll.shell32.ShellExecuteW(None, "runas", sys.executable, __file__, None, 1)